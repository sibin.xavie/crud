package com.employee.demo.dao;

import com.employee.demo.model.Employee;
import org.springframework.data.mongodb.repository.MongoRepository;

import org.springframework.stereotype.Repository;

@Repository
public interface InsertRepo extends MongoRepository<Employee,String>
{

}
