package com.employee.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.employee.demo.NextSequenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.employee.demo.dao.InsertRepo;
import com.employee.demo.model.Employee;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@RestController
public class ModelController {
    @Autowired
    InsertRepo repo;

    @Autowired
    NextSequenceService nextSequenceService;

    @RequestMapping("/add")
    public ModelAndView home() {
        ModelAndView mv = new ModelAndView("home");
        mv.addObject("SEARCH", "ADD");
        mv.setViewName("home");
        return mv;
    }

    @RequestMapping("/search")
    public ModelAndView search() {
        ModelAndView mv = new ModelAndView("showEmp");
        mv.addObject("SEARCH", "HAI");
        mv.setViewName("showEmp");
        return mv;
    }

    @DeleteMapping("/emp/{aid}")
    public String deleteEmployee(@PathVariable String aid) {
        Optional<Employee> byId = repo.findById(aid);
        repo.delete(byId.get());
        return "deleted";
    }

    private static final String HOSTING_SEQ_KEY = "aid";

    @PostMapping(path = "/addEmp")
    public ModelAndView addEmpSibin(@Valid Employee employee) {
        Employee save = null;
        ModelAndView mv = null;
        try {
            mv = new ModelAndView("showEmp");
            System.out.println("Inside addEmp: ");
            List<Employee> nextEmployee = nextSequenceService.getNextEmployee(employee.getName());
            if (nextEmployee.size() > 0) {
                mv.addObject("error", "Already exists");
                throw new Exception("Already exist");
            } else {
                repo.save(employee);
                ArrayList<Employee> all = (ArrayList<Employee>) repo.findAll();

                System.out.println(all.get(0).getName());

                mv.addObject("empList", all);
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return mv;
    }

    @GetMapping(path = "/getAll")
    public ModelAndView getAllEmployees() {
        ModelAndView mv = new ModelAndView("showEmp");
        mv.addObject("empList", repo.findAll());
        return mv;

    }

    @PutMapping(path = "/updateEmp")
    public Employee saveOrUpdateEmp(@RequestBody Employee employee) {
        repo.save(employee);
        return employee;
    }


    @RequestMapping("/getEmp")
    public ModelAndView getEmpOne(@RequestParam("empName") String empName) {
        System.out.println("In here" + empName);

        ModelAndView mv = new ModelAndView("showEmp");
        mv.addObject("empList", nextSequenceService.getNextEmployee(empName));
        return mv;
    }
}
