package com.employee.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {
		DataSourceAutoConfiguration.class})
public class SimpleStartCrud {
	public static void main(String[] args) {
		SpringApplication.run(SimpleStartCrud.class, args);
	}
}
