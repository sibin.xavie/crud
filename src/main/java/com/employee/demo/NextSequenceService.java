package com.employee.demo;

import com.employee.demo.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public class NextSequenceService {
    @Autowired
    private MongoOperations mongoOperation;
    public static final String COLLECTION_NAME = "emp_record";

    public List<Employee> getNextEmployee(String empName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("emp_name").is(empName));
        List<Employee> users = mongoOperation.find(query, Employee.class);


        return users;
    }


}
