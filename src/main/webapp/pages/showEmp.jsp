<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored = "false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>View Books</title>
        <link href="<c:url value="/css/common.css"/>" rel="stylesheet" type="text/css">
    </head>
    <body>
    ${error}
 <c:if test="${SEARCH eq 'HAI'}">
 <form action="/getEmp">
    <label>Search</label><input type = 'text' name="empName"><input type = 'submit'>
    </form>
  </c:if>
  <c:if test="${not empty empList}">

     <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>DOB</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${empList}" var="emp">
                        <tr>
                            <td>${emp.name}</td>
                            <td>${emp.surname}</td>
                            <td>${emp.dateOfBirth}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
    </c:if>

    </body>
</html>